const HtmlWebPackPlugin = require("html-webpack-plugin");
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
// const WorkboxPlugin = require('workbox-webpack-plugin');
// const WebappWebpackPlugin = require('webapp-webpack-plugin');
const path = require('path');

const appConfig = {
  target: 'web',
  mode: 'development',
  entry: {
    app: `${path.resolve(__dirname, 'src')}/index.js`,
  },
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  optimization: {
    usedExports: true,
      splitChunks: {
        chunks: 'all'
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/, /test/],
        use: {
          loader: "babel-loader",
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"]
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        exclude: [/node_modules/, /test/],
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
              disable: true,
            },
          },
        ],
      }
    ]
  },
  stats: {
      colors: true
  },
  plugins: [
    new CleanWebpackPlugin('dist', {}),
    // new WebappWebpackPlugin({
    //   logo: './images/logo.png',
    //   favicons: {
    //     developerURL: 'swyser.app',
    //     appName: 'swyser-app',
    //     appDescription: 'swyser.app',
    //     developerName: 'Francois van der Merwe (swyserdev.co.za)',
    //     background: '#ddd',
    //     theme_color: '#333',
    //     icons: {
    //       coast: false,
    //       yandex: false
    //     }
    //   }
    // }),
    // new HtmlWebPackPlugin({
    //   title: 'swyser.app',
    //   hash: true,
    //   minify: true,
    //   template: "./src/index.html",
    //   filename: "./index.html"
    // }),
    // new MiniCssExtractPlugin({
    //   hash: true,
    //   minify: true,
    //   filename: "[name].css",
    //   chunkFilename: "[id].css"
    // }),
    // new WorkboxPlugin.GenerateSW({
    //   clientsClaim: true,
    //   skipWaiting: true,
    //   runtimeCaching: [{
    //     urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
    //     handler: 'cacheFirst',
    //     options: {
    //       cacheName: 'images',
    //       expiration: {
    //         maxEntries: 10,
    //       },
    //     },
    //   }],
    // })
  ]
};

module.exports = [ appConfig ];
